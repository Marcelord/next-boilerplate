This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

recommended use of NPM 

```bash
npm i
```
First, run the development server:

```bash
npm run dev
```

## code style guide

### general rules
----
- use functional components with hooks
- separate logic of the component into its folder to elementary part as hooks/utils/helpers/section
- do not over engineer components if you use another jsx "part" to make component with no props consider react.memo to perserve Virtual DOM as much we can
- avoid using useCallback and useMemo with empty depenedency, in this cas function can be declared outside component
- styled components should be under the component declaration or in separate file inside component folder
- use typescript types avoid to ANY

### redux rules
----
separate reducers and its actions by feature!
example users
- users.reducer.ts
- users.actions.ts
- users.sagas.ts / if saga is in project

### SSR rules
----
get initial props are deprecated
strongly recommending of use getStaticProps with simple data fetch without access to server router, or serverSideProps in other case

visit @https://nextjs.org/docs/basic-features/data-fetching#typescript-use-getstaticprops

```ts
import { GetStaticProps } from 'next'

interfaceOfProps {
    prop1:string;
}

export const getStaticProps: GetStaticProps<interfaceOfProps> = async (context) => {
  // ...

  return {
      props: {
          prop1: 'hello world'
      }
  }
}


import { GetServerSideProps } from 'next'

export const getServerSideProps: GetServerSideProps = async (context) => {
  // ...
}

```

these functions MUST be called inside page file, for sake of readability declare them under the page component

----

## Page component rules
 - each component will be clean and no longer than 50 lines
 - every JSX components and logic except getserverside or getstatic props will be imported from other folders etc. utils, components, animations
 - getServerSideProps will be short and clear, ie. function inside this method will be declared in separate folders as well
 