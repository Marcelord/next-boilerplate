import React from 'react';
import { ThemeProvider } from 'styled-components';
import { theme } from '../styles/theme/theme';
import { GlobalStyle } from '../styles/GlobalStyle';
import { wrapper } from '../reduxStore/store';

function MyApp({ Component, pageProps }) {
    return (
        <ThemeProvider theme={theme}>
            <GlobalStyle />
            <Component {...pageProps} />;
        </ThemeProvider>
    );
}

export default wrapper.withRedux(MyApp);
