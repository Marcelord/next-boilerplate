import { NextPage } from 'next';
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { wrapper } from '../reduxStore/store';
import {
    serverRenderClock,
    startClock,
} from '../reduxStore/boilerplate/boilerplate.action';

interface HomeProps {
    test: string;
}

const Home: NextPage<HomeProps> = ({ test }) => {
    const dispatch = useDispatch();

    useEffect(() => {
        const timer = dispatch(startClock());

        return () => {
            clearInterval(timer as any);
        };
    }, [dispatch]);

    return (
        <div>
            <h2>{test}</h2>
        </div>
    );
};

export default Home;

export const getStaticProps = wrapper.getStaticProps(async ({ store }) => {
    store.dispatch(serverRenderClock(true));

    return {
        props: { test: 'me' },
    };
});
