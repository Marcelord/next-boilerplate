import { combineReducers } from 'redux';
import {
    boilerReducer,
    ITickInitialState,
} from './boilerplate/boilerplate.reducer';

export interface ICombinedState {
    boiler: ITickInitialState;
}

export const rootReducer = combineReducers({
    boiler: boilerReducer,
});
