import { Dispatch } from 'redux';

export enum tickActionTypes {
    TICK = 'TICK',
}

// note: both actions are can be written normal way ie not via currying

export const serverRenderClock = (isServer: boolean) => (
    dispatch: Dispatch
) => {
    return dispatch({
        type: tickActionTypes.TICK,
        light: !isServer,
        ts: Date.now(),
    });
};

export const startClock = () => (dispatch: Dispatch) => {
    return setInterval(
        () =>
            dispatch({
                type: tickActionTypes.TICK,
                light: true,
                ts: Date.now(),
            }),
        1000
    );
};


