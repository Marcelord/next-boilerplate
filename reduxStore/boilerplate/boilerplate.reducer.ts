import { tickActionTypes } from './boilerplate.action';
import produce from 'immer';

export interface ITickInitialState {
    lastUpdate: number;
    light: boolean;
}

const tickInitialState: ITickInitialState = {
    lastUpdate: 0,
    light: false,
};

export const boilerReducer = (state = tickInitialState, action) => {
    switch (action.type) {
        case tickActionTypes.TICK:
            return produce(tickInitialState, (nextState) => {
                nextState.lastUpdate = action.ts;
                nextState.light = !!action.light;
            });

        default:
            return state;
    }
};
