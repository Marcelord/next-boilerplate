import { createStore, applyMiddleware, Middleware } from 'redux';
import { HYDRATE, createWrapper } from 'next-redux-wrapper';
import thunkMiddleware from 'redux-thunk';
import { rootReducer, ICombinedState } from './rootReducer';

const middleWares = [thunkMiddleware];

const bindMiddleware = (middleware: Middleware[]) => {
    if (process.env.NODE_ENV !== 'production') {
        const { composeWithDevTools } = require('redux-devtools-extension');
        return composeWithDevTools(applyMiddleware(...middleware));
    }
    return applyMiddleware(...middleware);
};

const reducer = (state: ICombinedState, action) => {
    if (action.type === HYDRATE) {
        const nextState = {
            ...state, // use previous state
            ...action.payload, // apply delta from hydration
        };

        return nextState;
    } else {
        return rootReducer(state, action);
    }
};

const initStore = () => {
    return createStore(reducer, bindMiddleware(middleWares));
};

export const wrapper = createWrapper(initStore);
